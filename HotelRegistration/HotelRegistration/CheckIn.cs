﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HotelRegistration
{
    public partial class CheckIn : Form
    {
        public static string SetValueReservation = "";
        public CheckIn()
        {
            InitializeComponent();
        }

        private void CheckIn_Load(object sender, EventArgs e)
        {
            if (MessageAvailable.SetType.Equals("Assignment"))
            {
                txtInputDate.Enabled = false;
            }
        }

        private void btnCheckIn_Click(object sender, EventArgs e)
        {
            string first = txtFirst.Text;
            string last = txtLast.Text;
            string phone = txtPhone.Text;
            string email = txtEmail.Text;
            string InputDate = txtInputDate.Text;
            string OutputDate = txtOutputDate.Text;
            string type = "", state = "Busy";
            if (MessageAvailable.SetType.Equals("Assignment"))
            {
                type = "Normal";
            }
            else if (MessageAvailable.SetType.Equals("Reservation"))
            {
                type = "Reservation";
            }
            FuntionsDB.CheckInRoom(first, last, phone, email, InputDate, OutputDate, type);
            FuntionsDB.FuntionRoomState(state);
            SetValueReservation = FuntionsDB.SelectLastRoom(Rooms.SetValueRoom);
            Payment p = new Payment();
            this.Close();
            p.Show();
        }

        private void btnReturn_Click(object sender, EventArgs e)
        {
            MessageAvailable ma = new MessageAvailable();
            this.Close();
            ma.Show();
        }
    }
}
