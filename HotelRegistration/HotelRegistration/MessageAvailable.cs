﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HotelRegistration
{
    public partial class MessageAvailable : Form
    {
        CheckIn ci = new CheckIn();
        public static string SetType = "";
        public MessageAvailable()
        {
            InitializeComponent();
        }

        private void btnAss_Click(object sender, EventArgs e)
        {
            SetType = "Assignment";
            this.Close();
            ci.Show();
        }

        private void btnRes_Click(object sender, EventArgs e)
        {
            SetType = "Reservation";
            this.Close();
            ci.Show();
        }

        private void btnClean_Click(object sender, EventArgs e)
        {
            string state = "Cleaning";
            int result = FuntionsDB.FuntionRoomState(state);
            if (result == 1)
            {
                MessageBox.Show("UPDATE");
            }
            else
            {
                MessageBox.Show("NO UPDATE");
            }
            this.Close();
        }

        private void btnReturn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
