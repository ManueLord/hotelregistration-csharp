﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace HotelRegistration
{
    class FuntionsDB
    {
        public static SqlConnection Connection()
        {
            SqlConnection cn = new SqlConnection("SERVER=DESKTOP-K5F54V2;DATABASE=DBHotel;Integrated security=true");
            /*-------- Other option connection --------*/
            //
            cn.Open();
            return cn;
        }

        public static String[,] ShowData(String Command)
        {
            String[,] data = new String[25, 5];
            int x = 0;
            Connection();
            SqlCommand cmd = new SqlCommand(Command, Connection());
            SqlDataReader sdr = cmd.ExecuteReader();
            while (sdr.Read())
            {
                data[x, 0] = sdr["IdRooms"].ToString();
                data[x, 1] = sdr["name"].ToString();
                data[x, 2] = sdr["state"].ToString();
                data[x, 3] = sdr["cost"].ToString();
                data[x, 4] = sdr["type"].ToString();
                x++;
            }
            return data;
        }

        public static int FuntionRoomState(string state)
        {
            Connection();
            string command = "UPDATE rooms SET state = @state WHERE IdRooms = @IdRooms";
            SqlCommand cmd = new SqlCommand(command, Connection());
            cmd.Parameters.AddWithValue("@state", state);
            cmd.Parameters.AddWithValue("@IdRooms", Rooms.SetValueRoom);
            int result = cmd.ExecuteNonQuery();
            return result;
        }

        public static void CheckInRoom(string first, string last, string phone, string email, string InputDate, string OutputDate, string type)
        {
            Connection();
            string command = "INSERT INTO clients (first_name, last_name, phone, email, start_date, finish_date, type, room) VALUES (@first_name, @last_name, @phone, @email, @start_date, @finish_date, @type, @room)";
            SqlCommand cmd = new SqlCommand(command, Connection());
            cmd.Parameters.AddWithValue("@first_name", first);
            cmd.Parameters.AddWithValue("@last_name", last);
            cmd.Parameters.AddWithValue("@phone", phone);
            cmd.Parameters.AddWithValue("@email", email);
            cmd.Parameters.AddWithValue("@start_date", InputDate);
            cmd.Parameters.AddWithValue("@finish_date", OutputDate);
            cmd.Parameters.AddWithValue("@type", type);
            cmd.Parameters.AddWithValue("@room", Rooms.SetValueRoom);
            cmd.ExecuteNonQuery();
        }

        public static string SelectLastRoom(string room)
        {
            Connection();
            string command = "SELECT TOP 1 IdReservation FROM clients WHERE room = @room ORDER BY IdReservation DESC", data = "";
            SqlCommand cmd = new SqlCommand(command, Connection());
            cmd.Parameters.AddWithValue("@room", room);
            SqlDataReader sdr = cmd.ExecuteReader();
            if (sdr.Read())
            {
                data = sdr["IdReservation"].ToString(); 
            }
            return data;
        }

        public static int CostRoom(string IdReservation)
        {
            Connection();
            int total = 0;
            string command = "SELECT DATEDIFF (day, start_date, finish_date) as days FROM clients WHERE IdReservation = @IdReservation";
            SqlCommand cmd = new SqlCommand(command, Connection());
            cmd.Parameters.AddWithValue("@IdReservation", IdReservation);
            SqlDataReader sdr = cmd.ExecuteReader();
            if (sdr.Read())
            {
                string d = sdr["days"].ToString();
                total = Int32.Parse(d);
            }
            return total;
        }
    }
}
