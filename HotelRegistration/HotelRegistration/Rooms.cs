﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HotelRegistration
{
    public partial class Rooms : Form
    {
        private static bool _exiting;
        public static string SetValueRoom = "";
        public static string[] SetValueRoomCost = new string[5];
        public static String[,] rooms = new String[25,5];

        public Rooms()
        {
            InitializeComponent();
            
        }

        Button AddButton(int x)
        {
            Button btn = new Button();
            btn.Margin = new Padding(10);
            btn.Tag =  rooms[x, 0];
            btn.Text = rooms[x, 1] + "\nCost: " + rooms[x, 3];
            btn.Name = rooms[x, 2];
            btn.Width = 90;
            btn.Height = 90;
            switch (rooms[x, 2])
            {
                case "Available":
                    btn.BackColor = Color.Aquamarine;
                    break;
                case "Cleaning":
                    btn.BackColor = Color.BurlyWood;
                    break;
                case "Busy":
                    btn.BackColor = Color.DarkGreen;
                    break;
            }
            return btn;
        }

        void btnClick(object sender, EventArgs e)
        {
            DialogResult dr = new DialogResult();
            Button btn = sender as Button;
            SetValueRoom = btn.Tag.ToString();
            SetValueRoomCost = btn.Text.ToString().Split(' ');

            if (btn.Name == "Available")
            {
                MessageAvailable ma = new MessageAvailable();
                ma.Show();
            }
            else
            {
                if (btn.Name == "Cleaning")
                {
                    dr = MessageBox.Show("It's ready ?", "Message Clean", MessageBoxButtons.YesNo);
                    if (dr == DialogResult.Yes)
                    {
                        string state = "Available";
                        FuntionsDB.FuntionRoomState(state);
                    }
                }
                if (btn.Name == "Busy")
                {
                    dr = MessageBox.Show("The room is already vacated ? ", "Occupied Room", MessageBoxButtons.YesNo);
                    if (dr == DialogResult.Yes)
                    {
                        string state = "Cleaning";
                        FuntionsDB.FuntionRoomState(state);
                    }
                }
                this.Hide();
                this.Show();
            }
        }

        private void Rooms_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!_exiting && MessageBox.Show("Are you sure want to exit?",
                      "Hotel",
                       MessageBoxButtons.OKCancel,
                       MessageBoxIcon.Information) == DialogResult.OK)
            {
                _exiting = true;
                Environment.Exit(1);
            }
        }

        private void Rooms_Activated(object sender, EventArgs e)
        {
            Table.Controls.Clear();
            Table.RowCount = 5;
            Table.ColumnCount = 5;
            try
            {
                int x = 0;
                FuntionsDB.Connection();
               //MessageBox.Show("Connector Correct");
                String command = "SELECT * FROM rooms";
                rooms = FuntionsDB.ShowData(command);
                for (int i = 0; i < Table.RowCount; i++)
                {
                    for (int j = 0; j < Table.ColumnCount; j++)
                    {
                        Button btn = AddButton(x);
                        btn.Click += new EventHandler(this.btnClick);
                        Table.Controls.Add(btn, j, i);
                        x++;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
    }
}
