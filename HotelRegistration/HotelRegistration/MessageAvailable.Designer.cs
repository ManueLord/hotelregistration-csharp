﻿namespace HotelRegistration
{
    partial class MessageAvailable
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btnAss = new System.Windows.Forms.Button();
            this.btnRes = new System.Windows.Forms.Button();
            this.btnClean = new System.Windows.Forms.Button();
            this.btnReturn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(96, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Select option:";
            // 
            // btnAss
            // 
            this.btnAss.BackColor = System.Drawing.Color.Coral;
            this.btnAss.Location = new System.Drawing.Point(13, 51);
            this.btnAss.Name = "btnAss";
            this.btnAss.Size = new System.Drawing.Size(105, 23);
            this.btnAss.TabIndex = 1;
            this.btnAss.Text = "Room assignment";
            this.btnAss.UseVisualStyleBackColor = false;
            this.btnAss.Click += new System.EventHandler(this.btnAss_Click);
            // 
            // btnRes
            // 
            this.btnRes.BackColor = System.Drawing.Color.Peru;
            this.btnRes.Location = new System.Drawing.Point(124, 50);
            this.btnRes.Name = "btnRes";
            this.btnRes.Size = new System.Drawing.Size(81, 23);
            this.btnRes.TabIndex = 2;
            this.btnRes.Text = "Reservation";
            this.btnRes.UseVisualStyleBackColor = false;
            this.btnRes.Click += new System.EventHandler(this.btnRes_Click);
            // 
            // btnClean
            // 
            this.btnClean.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnClean.Location = new System.Drawing.Point(211, 51);
            this.btnClean.Name = "btnClean";
            this.btnClean.Size = new System.Drawing.Size(86, 23);
            this.btnClean.TabIndex = 3;
            this.btnClean.Text = "Cleaning";
            this.btnClean.UseVisualStyleBackColor = false;
            this.btnClean.Click += new System.EventHandler(this.btnClean_Click);
            // 
            // btnReturn
            // 
            this.btnReturn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReturn.Location = new System.Drawing.Point(13, 13);
            this.btnReturn.Name = "btnReturn";
            this.btnReturn.Size = new System.Drawing.Size(19, 23);
            this.btnReturn.TabIndex = 4;
            this.btnReturn.Text = "↩";
            this.btnReturn.UseVisualStyleBackColor = true;
            this.btnReturn.Click += new System.EventHandler(this.btnReturn_Click);
            // 
            // MessageAvailable
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(309, 86);
            this.Controls.Add(this.btnReturn);
            this.Controls.Add(this.btnClean);
            this.Controls.Add(this.btnRes);
            this.Controls.Add(this.btnAss);
            this.Controls.Add(this.label1);
            this.Name = "MessageAvailable";
            this.Text = "MessageAvailable";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnAss;
        private System.Windows.Forms.Button btnRes;
        private System.Windows.Forms.Button btnClean;
        private System.Windows.Forms.Button btnReturn;
    }
}