﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HotelRegistration
{
    public partial class Payment : Form
    {
        public Payment()
        {
            InitializeComponent();
        }

        private void Payment_Load(object sender, EventArgs e)
        {
            int days = 0, cost = 0, total = 0;
            days = FuntionsDB.CostRoom(CheckIn.SetValueReservation);
            cost = Int32.Parse(Rooms.SetValueRoomCost[2]);
            total = days * cost;
            txtDays.Text = days.ToString();
            txtCost.Text = cost.ToString();
            txtTotal.Text = total.ToString();
        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
